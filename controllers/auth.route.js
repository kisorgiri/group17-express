const express = require('express');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const router = express.Router();
const UserModel = require('./../models/user.model');
//sub route is defined here
const config = require('./../config');
const map_user = require('./../helpers/user.map');
const nodemailer = require('nodemailer');

const sender = require('./../config/nodemailer.config');
function prepareMail(data) {
    var mailBody = {
        from: 'Smart Web Store <noreply@abcd.com>', // sender address
        to: "dhakallaxman23@gmail.com,tansha.mhr@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<p>Hello <b>${data.name},</b></p>
        <p>We noticed that you are having trouble logging into our system, please click the link below to reset your password.</p>
        <p><a href="${data.link}">reset password</a></p>
        <p>If you have not requested for resetting your password kindly ignore this email</p>
        <p>Regards,</p>
        <p>Smart Support Team</p>` // html body
    }
    return mailBody;
}


router.get('/', function (req, res, next) {
    require('fs').readFile('sdlf', function (err, data) {
        if (err) {
            return req.myEvent.emit('error', err);
        }
    })
});

router.post('/', function (req, res, next) {
    UserModel.findOne({
        $or: [{
            username: req.body.username
        }, {
            email: req.body.username
        }]
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // user will have hashed password
                // input ma aayeko password pani huncha
                var isMatch = passwordHash.verify(req.body.password, user.password);
                if (isMatch) {
                    var token = jwt.sign({ id: user._id }, config.jwtSecret);
                    res.status(200).json({
                        user: user,
                        token: token
                    });
                } else {
                    next({
                        msg: 'invalid username or password'
                    });
                }
            } else {
                next({
                    msg: 'invalid username or password'
                });
            }
        })
})


router.post('/register', function (req, res, next) {
    console.log('i am at post request of register', req.body);
    var newUser = new UserModel({});
    var newMappedUser = map_user(newUser, req.body);
    console.log('new user >>', newUser);
    // var obj = {name:'brodway'};
    // obj.name = 'infosys';
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200).json(done);
    })

});

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Email is not registered yet"
                });
            }

            var mailData = {
                name: user.username,
                email: user.email,
                link: req.headers.origin + '/auth/reset/' + user._id,
            }
            var email = prepareMail(mailData);
            user.passwordResetExpiry = new Date(Date.now() + 1000 * 60 * 60 * 24 * 2);
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(email, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                });
            });
            // send email
        })
})

router.post('/reset-password/:token', function (req, res, next) {
    var token = req.params.token;

    UserModel.findOne({
        _id: token,
        passwordResetExpiry: { $gte: Date.now() }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'password reset token is invalid expired'
                })
            }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})

module.exports = router;