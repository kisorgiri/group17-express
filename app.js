const express = require('express');
const morgan = require('morgan');
const app = express(); // now this app constant hold entire express framework
var path = require('path');
app.set('port', 9090)
var port = app.get('port');
require('./db');
const cors = require('cors');
const socket = require('socket.io');
// socket stuff
var users = [];
const io = socket(app.listen(9091));
io.on('connection', function (client) {
    console.log("client conected to socket server");
    var id = client.id;
    client.on('new-user', function (data) {
        users.push({
            id: id,
            name: data
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })
    client.on('new-msg', function (data) {
        client.broadcast.to(data.receiverId).emit('reply-msg', data); // aafu bahek aru connected client lai
        client.emit('reply-msg-own', data); // affulai matrai
    })

    client.on('disconnect', function () {
        users.forEach(function (item, i) {
            if (item.id === id) {
                users.splice(i, 1);
            }
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })

})

const event = require('./events');
event.on('product', function (data) {
    console.log('products data >>', data);
});

// template engine setup
app.set('view-engine', require('pug'));
app.set('views', path.join(__dirname, 'views'));
// load routng level middleware
const apiRouter = require('./routes/api.route')();

// application level middleware
const authentication = require('./middlewares/authenticate');
const authorization = require('./middlewares/authorize');

app.use(function (req, res, next) {
    req.myEvent = event;
    next();
})
// load third party middleware
app.use(morgan('dev'));

// laod inbuilt middleware
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
// console.log('__dir name >>>', __dirname);
app.use(cors());

app.use(express.static('files')); // internally view file le access garnu paryo vane
app.use('/file', express.static(path.join(__dirname, 'files'))); // external client endpoint provided to  view file



// app.use(authentication);
// app.use(authorization);
app.use('/api', apiRouter);

app.use(function (req, res, next) {
    console.log('application level middleware below routiing configureation');
    // res.json({
    //     msg: "I am 404 Error Handler "
    // })
    next({
        status: 404,
        msg: 'Not Found'
    });
});

event.on('error', function (err) {
    console.log('error from entire application ', err);
})
app.use(function (err, req, res, next) {
    console.log('i am error handling middleware', err);
    res.status(err.status || 400).json({
        msg: 'from error handling middleware',
        err: err
    })
});

app.listen(port, function (err, done) {

    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + port);
        console.log('press CTRL +C to exit');
    }
})


// global keyword in nodejs
// process, global, __dirname, __filename


// middleware middleware is a function that has access to http request object http response object
// and next middleware refrence
// middleware can access and modify http request object
// middleware can access and modify http response object
// middleware came into action in between http request response cycle
// http request response cycle
// MIDDLEWARE ko order is very important
// syntax
// function(req,res,next){
// // 1st argument is http request object
// // 2nd argument is http response object
// // 3rd argument is next middleware refrence
// }

// configuration 
// app.use() // app.use is configuration block for middleware

// types of middleware
// application level middleware
// routing level middleware
// third party middleware
// inbuilt middleware
// error handling middleware