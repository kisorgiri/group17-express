const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/group17db', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.once('open', function () {
    console.log('database connection open');
});
mongoose.connection.on('err', function (err) {
    console.log('db connection failed');
});